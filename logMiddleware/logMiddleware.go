package logmiddleware

import (
	"log"
	"net/http"
	"time"
)

type LogMiddleware struct {
	next http.Handler
}

var _ http.Handler = (*LogMiddleware)(nil)

func CrateLogMiddleware(next http.Handler) *LogMiddleware {
	return &LogMiddleware{
		next: next,
	}
}

func (lm *LogMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	startTime := time.Now()

	lm.next.ServeHTTP(w, r)

	log.Println(r.URL)
	log.Printf("since: %s", time.Since(startTime))
}
