package main

import (
	"fmt"
	"net/http"

	"github.com/caarlos0/env"
	logmiddleware "gitlab.com/volodya_melnyk/test-go3/logMiddleware"
	roothandler "gitlab.com/volodya_melnyk/test-go3/rootHandler"
)

type Config struct {
	PORT string `env:"PORT" envDefault:"3000"`
}

func main() {
	cfg := Config{}
	if err := env.Parse(&cfg); err != nil {
		panic(err)
	}

	mux := http.NewServeMux()
	rootHandler := roothandler.CreateRootHandler()
	logMiddleware := logmiddleware.CrateLogMiddleware(rootHandler)

	mux.Handle("/", logMiddleware)

	addr := fmt.Sprintf(":%s", cfg.PORT)

	fmt.Printf("Server listen at: %s\n", addr)
	http.ListenAndServe(addr, mux)
}
