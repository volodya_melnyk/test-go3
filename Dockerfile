FROM golang:1.22.2 as builder

WORKDIR /app

ADD go.mod .
ADD go.sum .

COPY logMiddleware /app/logMiddleware
COPY rootHandler /app/rootHandler

ADD main.go /app/main.go

RUN go mod tidy

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build

CMD ["sh"]

FROM alpine

WORKDIR /app

COPY --from=builder /app/test-go3 /app

CMD ["./test-go3"]
