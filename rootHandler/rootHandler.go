package roothandler

import (
	"encoding/json"
	"net/http"
	"time"
)

type Handler struct{}
type HttpResponse struct {
	Msg  string    `json:"msg"`
	Time time.Time `json:"time"`
}

var _ http.Handler = (*Handler)(nil)

func createHttpResponse(msg string) *HttpResponse {
	return &HttpResponse{
		Msg:  msg,
		Time: time.Now().UTC(),
	}
}

func (hr *HttpResponse) toBinary() ([]byte, error) {
	response, err := json.Marshal(hr)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func CreateRootHandler() *Handler {
	return &Handler{}
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	response := createHttpResponse("Hello world")
	bResponse, err := response.toBinary()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	w.Header().Add("Content-Type", "application/json")
	w.Write(bResponse)

}
